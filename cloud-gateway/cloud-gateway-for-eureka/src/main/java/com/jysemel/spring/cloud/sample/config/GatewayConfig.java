package com.jysemel.spring.cloud.sample.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jysemel
 */
@Configuration
public class GatewayConfig {

    @Bean
    public RouteLocator customerRoute(RouteLocatorBuilder routeLocatorBuilder){

        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();

        routes.route("pp",r->r.path("/guonei").uri("http://news.baidu.com/guonei"));

        return routes.build();
    }

//    @Bean
//    public RouteLocator customerRoute1(RouteLocatorBuilder routeLocatorBuilder){
//
//        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
//
//        routes.route("pp1",r->r.path("/tet").uri("http://localhost:7003/search/getFeign"));
//
//        return routes.build();
//    }
}