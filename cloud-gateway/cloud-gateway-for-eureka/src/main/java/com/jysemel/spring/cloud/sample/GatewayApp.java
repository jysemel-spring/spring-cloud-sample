package com.jysemel.spring.cloud.sample;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author jysemel
 */
@EnableEurekaClient
@SpringBootApplication
public class GatewayApp {

    public static void main( String[] args ){
        SpringApplication.run(GatewayApp.class);
        System.out.println("GatewayApp Hello World!" );
    }
}