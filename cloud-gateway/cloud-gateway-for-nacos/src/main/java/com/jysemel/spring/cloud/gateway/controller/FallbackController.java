package com.jysemel.spring.cloud.gateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: jysemel
 * @Date: 2021/7/25 14:49
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc
 */
@RestController
public class FallbackController {

    @GetMapping("/fallbackA")
    public String fallbackA() {
        return "服务暂时不可用";
    }
}