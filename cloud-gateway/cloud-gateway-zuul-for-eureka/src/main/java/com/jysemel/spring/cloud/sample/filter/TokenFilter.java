package com.jysemel.spring.cloud.sample.filter;

import com.jysemel.spring.cloud.sample.config.ZuulConfig;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@Slf4j
@Component
public class TokenFilter extends ZuulFilter {

    @Autowired
    private ZuulConfig zuulConfig;


    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        //实现逻辑
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        String token = request.getParameter("token");
        if (Objects.isNull(token)){
            log.error("当前访问没有权限.................");
            requestContext.setSendZuulResponse(false);
            requestContext.setResponseStatusCode(HttpStatus.UNAUTHORIZED.hashCode());
        }
        log.info(zuulConfig.getDddd());
        requestContext.set("startTime",System.currentTimeMillis());
        return null;
    }

}