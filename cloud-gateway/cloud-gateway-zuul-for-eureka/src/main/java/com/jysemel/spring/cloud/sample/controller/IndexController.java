package com.jysemel.spring.cloud.sample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jysemel
 */
@RequestMapping
@RestController
public class IndexController {


    @GetMapping
    public String index(){
        return "hello";
    }
}