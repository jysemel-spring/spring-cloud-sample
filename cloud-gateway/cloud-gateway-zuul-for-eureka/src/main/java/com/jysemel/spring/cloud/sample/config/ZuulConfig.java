package com.jysemel.spring.cloud.sample.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@Data
@Component
@ConfigurationProperties("tuname")
public class ZuulConfig {

    private String dddd;
}
