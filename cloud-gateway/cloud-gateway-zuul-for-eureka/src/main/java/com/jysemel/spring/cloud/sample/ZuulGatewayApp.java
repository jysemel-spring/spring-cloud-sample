package com.jysemel.spring.cloud.sample;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author jysemel
 */
@EnableEurekaClient
@EnableZuulProxy
@SpringBootApplication
public class ZuulGatewayApp {

    public static void main( String[] args ){
        SpringApplication.run(ZuulGatewayApp.class);
        System.out.println("ZuulGatewayApp Hello World!" );
    }

}