package com.jysemel.spring.cloud.sample.controller;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class HealthChecker implements HealthIndicator {

    private boolean up = true;

    @Override
    public Health health() {
        if (up) {
            //自定义监控内容
            return new Health.Builder().withDetail("status", "up").up().build();
        } else {
            return new Health.Builder().withDetail("error", "rpc is down").down().build();
        }
    }

    public boolean isUp() {
        return up;
    }

    public void setUp(boolean up) {
        this.up = up;
    }
}