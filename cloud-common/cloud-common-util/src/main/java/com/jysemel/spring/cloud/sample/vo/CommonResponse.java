package com.jysemel.spring.cloud.sample.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * des:
 * 通用响应response
 * @author jysemel
 * @date 2019/03/23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResponse <T> implements Serializable {

    private T data;
    private String msg;
    private Boolean success;

    public static <T> CommonResponse<T> success() {
        return new CommonResponse<T>(null, null, true);
    }

    public static <T> CommonResponse<T> success(T data) { return new CommonResponse<T>(data, null, true); }

    public static <T> CommonResponse<T> success(String msg) { return new CommonResponse<T>(null, msg, true); }

    public static <T> CommonResponse<T> success(T data, String msg) { return new CommonResponse<T>(data, msg, true); }

    public static <T> CommonResponse<T> error() { return new CommonResponse<T>(null, null, false); }

    public static <T> CommonResponse<T> error(T data) { return new CommonResponse<T>(data, null, false); }

    public static <T> CommonResponse<T> error(String msg) { return new CommonResponse<T>(null, msg, false); }

    public static <T> CommonResponse<T> error(T data, String msg) { return new CommonResponse<T>(data, msg, false); }

}