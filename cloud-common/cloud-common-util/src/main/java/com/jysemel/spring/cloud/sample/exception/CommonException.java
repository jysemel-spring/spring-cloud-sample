package com.jysemel.spring.cloud.sample.exception;

/**
 * des:
 * 通用异常定义
 * @author jysemel
 * @date 2019/03/24
 */
public class CommonException extends Exception{

    public CommonException(String msg){
        super(msg);
    }
}