package com.jysemel.spring.cloud.sample.utils;


import org.apache.commons.codec.digest.DigestUtils;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/03/24
 */
public class CommonUtils {

    public static String md5(String value){
        return DigestUtils.md5Hex(value).toUpperCase();
    }
}