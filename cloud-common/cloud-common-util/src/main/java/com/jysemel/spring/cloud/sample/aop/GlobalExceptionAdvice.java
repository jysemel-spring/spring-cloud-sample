package com.jysemel.spring.cloud.sample.aop;

import com.jysemel.spring.cloud.sample.exception.CommonException;
import com.jysemel.spring.cloud.sample.vo.CommonResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * des:
 *全局异常
 * @author jysemel
 * @date 2019/03/24
 */
@RestControllerAdvice
public class GlobalExceptionAdvice {

    @ExceptionHandler(value = CommonException.class)
    public CommonResponse<String> handlerException(HttpServletRequest req, CommonException come){
        CommonResponse<String> response = CommonResponse.error();
        response.setData(come.getMessage());
        return response;
    }
}