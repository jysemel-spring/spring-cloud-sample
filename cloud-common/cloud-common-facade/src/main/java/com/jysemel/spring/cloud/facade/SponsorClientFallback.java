package com.jysemel.spring.cloud.facade;


import com.jysemel.spring.cloud.facade.vo.HelloModel;
import com.jysemel.spring.cloud.sample.vo.CommonResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;


/**
 * @author jysemel
 */
@Component
public class SponsorClientFallback implements SponsorClient{

    @Override
    @RequestMapping(value = "/sponsor/list",method = RequestMethod.POST)
    public CommonResponse<List<HelloModel>> listHello(){
        return CommonResponse.success();
    }
}
