package com.jysemel.spring.cloud.facade.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/03/24
 */
@Data
public class HelloModel implements Serializable {

    private Long id;
    private String title;
    private String text;
}