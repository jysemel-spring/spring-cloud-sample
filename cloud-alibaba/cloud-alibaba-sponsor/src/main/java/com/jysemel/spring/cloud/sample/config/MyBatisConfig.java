package com.jysemel.spring.cloud.sample.config;

import com.baomidou.mybatisplus.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/03/24
 */
@Configuration
@MapperScan(basePackages = {"com.jysemel.spring.cloud.sample.**.mapper"})
public class MyBatisConfig {

    /**
     * mybatis-plus SQL执行效率插件【生产环境可以关闭】
     */
    @Bean
    public PerformanceInterceptor performanceInterceptor() {

        return new PerformanceInterceptor();
    }
}