package com.jysemel.spring.cloud.sample.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jysemel.spring.cloud.sample.mapper.HelloMapper;
import com.jysemel.spring.cloud.sample.model.HelloModel;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/03/24
 */
@Service
public class HelloService extends ServiceImpl<HelloMapper, HelloModel> {

    public List<HelloModel> selectAll() {

        return this.baseMapper.selectAll();
    }

}