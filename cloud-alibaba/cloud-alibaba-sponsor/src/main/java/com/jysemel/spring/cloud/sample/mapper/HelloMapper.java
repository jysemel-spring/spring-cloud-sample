package com.jysemel.spring.cloud.sample.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jysemel.spring.cloud.sample.model.HelloModel;

import java.util.List;

/**
 * @author jysemel
 */
public interface HelloMapper extends BaseMapper<HelloModel> {


    public List<HelloModel> selectAll();
}