package com.jysemel.spring.cloud.sample.rpc;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author jysemel
 */
@FeignClient(value = "crm-hw")
public interface CrmClient {

    @RequestMapping(value = "/customer/high/highSeas/queryCallInShop",method = RequestMethod.POST)
    JSONObject listHello(@RequestHeader("TRACE_LOG_ID") String TRACE_LOG_ID,@RequestBody JSONObject json);
}
