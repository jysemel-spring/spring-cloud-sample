package com.jysemel.spring.cloud.sample.controller;

import com.alibaba.fastjson.JSON;
import com.jysemel.spring.cloud.sample.rpc.SponsorClient;
import com.jysemel.spring.cloud.sample.rpc.vo.HelloModel;
import com.jysemel.spring.cloud.sample.vo.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/03/24
 */
@Slf4j
@RestController
public class SearchController {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private SponsorClient sponsorClient;
    @Autowired
    private LoadBalancerClient loadBalancerClient;

    /**
     * 松散型  @LoadBalanced bean实例注解负载型
     * @return
     */
    @GetMapping("getRestTemplate")
    public CommonResponse<List<HelloModel>> getRestTemplateTest() {
        String str = restTemplate.postForEntity(
                "http://sponsor/sponsor/list",
                null,
                String.class
        ).getBody();
        CommonResponse<List<HelloModel>> helloModels = JSON.parseObject(str,CommonResponse.class);
        log.info("search: getRestTemplate -> {}",str);
        log.info("search: getRestTemplate -> {}",JSON.toJSONString(helloModels));
        return helloModels;
    }

    /**
     * 强 规范约束型
     * @return
     */
    @GetMapping("/getFeign")
    public CommonResponse<List<HelloModel>> getFeignTest() {
        CommonResponse<List<HelloModel>> list = sponsorClient.listHello();
        log.info("getFeign: get{}",JSON.toJSONString(list));
        return list;
    }

    /**
     * 负载型  不用bean 实例注解  @LoadBalanced
     * @return
     */
    @GetMapping("/loadBalancerClient")
    public CommonResponse<List<HelloModel>> loadBalancerClient() {
        ServiceInstance serviceInstance = loadBalancerClient.choose("sponsor");
        log.info("info host:{},post:{},instanceId:{}",serviceInstance.getHost(),serviceInstance.getPort(),serviceInstance.getInstanceId());
        String url = String.format("http://%s:%s",serviceInstance.getHost(),serviceInstance.getPort()+"/sponsor/list");
        log.info("url:{}",url);
        String sms = restTemplate.postForObject(url,null, String.class);
        log.info("search: loadBalancerClient -> {}",sms);
        CommonResponse<List<HelloModel>> helloModels = JSON.parseObject(sms,CommonResponse.class);
        log.info("search: loadBalancerClient -> {}",JSON.toJSONString(helloModels));
        return helloModels;
    }
}