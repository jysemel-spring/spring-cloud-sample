package com.jysemel.spring.cloud.sample.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jysemel.spring.cloud.sample.rpc.CrmClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jysemel
 */
@Slf4j
@RestController
@RequestMapping("crm")
public class CrmTest {

    @Autowired
    private CrmClient crmClient;

    @GetMapping
    public void test(){
        JSONObject jsonObject = new JSONObject();
        JSONObject list = crmClient.listHello("sdfsdfsdfsdf",jsonObject);
        log.info("getFeign: get{}", JSON.toJSONString(list));

    }
}