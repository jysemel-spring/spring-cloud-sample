package com.jysemel.spring.cloud.sample.rpc;


import com.jysemel.spring.cloud.sample.rpc.vo.HelloModel;
import com.jysemel.spring.cloud.sample.vo.CommonResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * @author jysemel
 */
@FeignClient(value = "sponsor")
public interface SponsorClient {

    @RequestMapping(value = "/sponsor/list",method = RequestMethod.POST)
    CommonResponse<List<HelloModel>> listHello();
}
