package com.jysemel.spring.cloud.sample.controller;

import com.alibaba.fastjson.JSON;
import com.jysemel.spring.cloud.sample.rpc.GatewayClient;
import com.jysemel.spring.cloud.sample.rpc.vo.HelloModel;
import com.jysemel.spring.cloud.sample.vo.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author jysemel
 */
@Slf4j
@RestController
@RequestMapping("gateway")
public class GatewayTest {

    @Autowired
    private GatewayClient gatewayClient;

    @GetMapping
    public void test(){
        CommonResponse<List<HelloModel>> list = gatewayClient.listHello();
        log.info("getFeign: get{}", JSON.toJSONString(list));

    }
}