package com.jysemel.dubbo.cloud.impl;

import com.jysemel.dubbo.cloud.ITest;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

@Service
@DubboService
public class ITestImpl implements ITest {

    @Override
    public void say() {
        System.out.println("=================00 ");
    }
}
