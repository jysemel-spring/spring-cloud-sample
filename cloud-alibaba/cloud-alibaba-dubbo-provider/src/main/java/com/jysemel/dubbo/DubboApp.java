package com.jysemel.dubbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author jysemel
 */
@EnableDiscoveryClient
@SpringBootApplication
public class DubboApp {

    public static void main( String[] args ){
        SpringApplication.run(DubboApp.class);
        System.out.println("DubboApp Hello World!" );
    }
}
