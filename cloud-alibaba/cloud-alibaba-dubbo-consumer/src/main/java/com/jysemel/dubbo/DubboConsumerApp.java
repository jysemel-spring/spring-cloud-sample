package com.jysemel.dubbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author jysemel
 */
@EnableDiscoveryClient
@SpringBootApplication
public class DubboConsumerApp {

    public static void main( String[] args ){
        SpringApplication.run(DubboConsumerApp.class);
        System.out.println("DubboConsumerApp Hello World!" );
    }
}
