# spring.cloud.sample

#### 介绍
spring.cloud.sample 小样
- 基于spring-boot (2.1.0.RELEASE) 版本
- 基于spring-cloud (Greenwich.RELEASE) 版本
- jdk (1.8) 版本

#### 软件架构
1. cloud-eureka-server 通过eureka注册中心
2. cloud-gateway 通过zuul提供api网关
   - RateLimiter 限流
   - Hystrix 降级熔断
3. service 业务服务模块
   - cloud-service-search    检索模块
   - cloud-service-sponsor   资源库
   - cloud-service-dashboard 监控模块
   - cloud-service-config    模拟获取配置中心信息
   - cloud-service-hystrix   服务容错
     - 容错
     - 超时处理
     - 熔断
     - hystrix-dashboard 可视化的管理界面
   - cloud-service-zipkin   日志追踪,服务追踪
     - traceId     
4. db 基于mybatis + sqlite
5. 应用间通信
   - http 
     - spring cloud
       - RestTemplate
       - Feign （伪rpc）
   - rpc
     - dubbo
   - ribbon 负载均衡  
6. 单元测试基于chome插件 Restlet Client
7. cloud-config 业务参数配置中心
   - 提供服务端和客户端支持
   - 集中管理各环境的配置文件
   - 配置文件修改之后，可以快速的生效
   - 可以进行版本管理
   - 支持大的并发查询
   - 支持各种语言

#### 安装教程

1. xxxx
2. xxxx
3. xxxx迭代

#### 使用说明

1. eureka 注册中心
2. java -jar cloud-eureka-server-1.0-SNAPSHOT.jar --spring.profiles.active=master
3. Hystrix 服务降级机制
4. zuul 网关路由
5. feign

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)