package com.jysemel.spring.cloud.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@EnableDiscoveryClient
@EnableCircuitBreaker
@SpringCloudApplication
public class BusApp {

    public static void main( String[] args ){
        SpringApplication.run(BusApp.class);
        System.out.println("BusApp Hello World!" );
    }

}