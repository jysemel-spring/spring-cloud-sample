package com.jysemel.cloud.components;

/**
 * @author: jysemel
 * @Date: 2021/7/26 13:31
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc
 */
public class ResponseObject {

    private String code;

    private String message;

    private Object data;



    public ResponseObject(String code, String message) {
        this.code = code;
        this.message = message;
    }
}