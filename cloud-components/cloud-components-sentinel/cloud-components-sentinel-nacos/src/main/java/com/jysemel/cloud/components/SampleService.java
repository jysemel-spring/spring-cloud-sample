package com.jysemel.cloud.components;

import org.springframework.stereotype.Service;

/**
 * @author: jysemel
 * @Date: 2021/7/26 13:31
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc
 */
@Service
public class SampleService {

    public void createOrder(){
        try {
            //模拟处理业务逻辑需要101毫秒
            Thread.sleep(101);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("订单已创建");
    }

}