package com.jysemel.cloud.components.controller;

import com.alibaba.csp.sentinel.SphO;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: jysemel
 * @Date: 2021/4/22 16:07
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc
 */
@RestController
public class ProController {


    @GetMapping("/demo")
    public String demo(){
        return "hello get";
    }
}