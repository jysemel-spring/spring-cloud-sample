package com.jysemel.cloud.components;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author jysemel
 */
@SpringBootApplication
public class SentinelApp {

    public static void main( String[] args ){
        SpringApplication.run(SentinelApp.class);
        System.out.println("SentinelApp Hello World!" );
    }
}