//package com.jysemel.spring.cloud.sample;
//
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import zipkin.server.EnableZipkinServer;
//
/**
// * @author jysemel
// */
//@EnableZipkinServer
//@SpringBootApplication
//public class EnableZipkinServerApp {
//
//    public static void main(String[] args) {
//        SpringApplication.run(EnableZipkinServerApp.class, args);
//    }
//}