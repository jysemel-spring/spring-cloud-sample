package com.jysemel.spring.cloud.sample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@RestController
public class ZipkinController {



    @GetMapping("list")
    public String getList(HttpServletRequest request) throws Exception {

        return "success";
    }
}