package com.jysemel.spring.cloud.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */

@SpringBootApplication
public class ZipkinApp {

    public static void main( String[] args ){
        SpringApplication.run(ZipkinApp.class);
        System.out.println("ZipkinApp Hello World!" );
    }
}
