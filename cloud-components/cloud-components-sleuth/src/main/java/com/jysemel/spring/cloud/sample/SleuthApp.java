package com.jysemel.spring.cloud.sample;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


/**
 * @author jysemel
 */
@SpringBootApplication
@RestController
public class SleuthApp {

    private static Logger log = LoggerFactory.getLogger(SleuthApp.class);


    private RestTemplate restTemplate;

    @RequestMapping("/")
    public String home() {
        log.info("Handling333 home11");
        return "Hello World11dfgf333dgfg33331";
    }

    public static void main(String[] args) {
        log.info("Application home");
        SpringApplication.run(SleuthApp.class, args);
    }

}