package com.jysemel.cloud.components.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: jysemel
 * @Date: 2021/7/26 11:44
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc
 */
@Configuration
@RefreshScope
@RestController
public class TestController {

    @Value("${custom.flag}")
    private String flag;



    @GetMapping("/test")
    public String test(){
        return "flag:" + flag ;
    }

}