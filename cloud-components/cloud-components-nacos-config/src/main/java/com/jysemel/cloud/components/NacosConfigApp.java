package com.jysemel.cloud.components;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: jysemel
 * @Date: 2021/7/26 11:41
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc
 */
@SpringBootApplication
public class NacosConfigApp {

    public static void main( String[] args ){
        SpringApplication.run(NacosConfigApp.class);
        System.out.println("NacosConfigApp Hello World!" );
    }
}
