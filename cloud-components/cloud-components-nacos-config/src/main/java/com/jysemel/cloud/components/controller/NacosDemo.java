package com.jysemel.cloud.components.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: jysemel
 * @Date: 2021/7/26 11:43
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc
 */
@RestController
public class NacosDemo {


    @GetMapping("demo")
    public String demo(){
        return "demo";
    }
}