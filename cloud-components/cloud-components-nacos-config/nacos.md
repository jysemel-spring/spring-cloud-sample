### nacos yaml
> 文件命名格式 微服务 id”-“环境名”.“文件扩展名” 三部分组合为有效的 data id
> 通过nacos服务端根据环境分别创建 nacos-config-dev.yml和nacos-config-prod.yml

- nacos-config-dev.yml

```
custom: #自定义配置项
  flag: dev
  database: 192.168.10.31
```
- nacos-config-prod.yml
```
custom: #自定义配置项
  flag: prod
  database: 192.168.10.31
```
