package com.jysemel.spring.cloud.sample.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@RestController
@DefaultProperties(defaultFallback = "defaultFallback")
public class HystrixController2 {



    @HystrixCommand()
    @GetMapping("default")
    public String getDefault() throws Exception {
        throw new Exception("test");
    }

    private String defaultFallback(){
        return "defaultFallback errorCallback";
    }
}