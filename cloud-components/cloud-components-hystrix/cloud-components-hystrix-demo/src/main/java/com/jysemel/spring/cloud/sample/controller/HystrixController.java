package com.jysemel.spring.cloud.sample.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@RestController
public class HystrixController {

    @HystrixCommand(fallbackMethod = "errorCallback")
    @GetMapping("list")
    public String getList(HttpServletRequest request) throws Exception {
        String test = request.getParameter("test");
        if (Objects.isNull(test)){
            throw new Exception("test");
        }
        return "success";
    }

    private String errorCallback(HttpServletRequest request){
        return "errorCallback-test";
    }
}