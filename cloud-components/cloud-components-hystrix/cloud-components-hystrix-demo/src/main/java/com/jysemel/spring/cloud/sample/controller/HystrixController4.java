package com.jysemel.spring.cloud.sample.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@RestController
public class HystrixController4 {



    @HystrixCommand(
        fallbackMethod = "getCall" ,
        commandProperties = {
                @HystrixProperty(name= HystrixPropertiesManager.EXECUTION_ISOLATION_THREAD_TIMEOUT_IN_MILLISECONDS,value = "5000"),
                @HystrixProperty(name= HystrixPropertiesManager.EXECUTION_ISOLATION_STRATEGY,value = "THREAD")
        },threadPoolProperties = {
                @HystrixProperty(name="coreSize",value = "1"),
                @HystrixProperty(name="maxQueueSize",value = "10")
        }
    )
    @GetMapping("param")
    public String get(HttpServletRequest request) throws Exception {
        String test = request.getParameter("test");
        Thread.sleep(TimeUnit.MINUTES.toMillis(6));
        return "success";
    }

    private String getCall(HttpServletRequest request){
        return "get timeOut.....";
    }
}