package com.jysemel.spring.cloud.sample.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@RestController
public class HystrixController3 {



    @HystrixCommand(
            groupKey = "",  //通过group key可以对命令方法进行分组，便于Hystrix数据统计、告警及dashboad展示
            commandKey = "", // 常用于对该命令进行动态参数设置
            threadPoolKey = "", // 标识命令所归属的线程池，具有相同threadPoolKey的命令使用同一个线程池
            fallbackMethod = "getCall" ,
            commandProperties = {
                    @HystrixProperty(name= HystrixPropertiesManager.EXECUTION_ISOLATION_THREAD_TIMEOUT_IN_MILLISECONDS,
                            value = "5000")
            })
    @GetMapping("get")
    public String get(HttpServletRequest request) throws Exception {
        String test = request.getParameter("test");
        Thread.sleep(TimeUnit.MINUTES.toMillis(6));
        return "success";
    }

    private String getCall(HttpServletRequest request){
        return "get timeOut.....";
    }
}