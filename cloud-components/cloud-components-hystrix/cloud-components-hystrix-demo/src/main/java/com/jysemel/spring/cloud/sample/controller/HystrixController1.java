package com.jysemel.spring.cloud.sample.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@RestController
public class HystrixController1 {



    @HystrixCommand(fallbackMethod = "timeOutCall" ,
            commandProperties = {
            @HystrixProperty(name= HystrixPropertiesManager.EXECUTION_ISOLATION_THREAD_TIMEOUT_IN_MILLISECONDS,
                    value = "5000")
    })
    @GetMapping("timeOut")
    public String timeOut(HttpServletRequest request) throws Exception {
        String test = request.getParameter("test");
        Thread.sleep(TimeUnit.MINUTES.toMillis(5));
        return "success";
    }

    private String timeOutCall(HttpServletRequest request){
        return "timeOut.....";
    }
}