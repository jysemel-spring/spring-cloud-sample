package com.jysemel.spring.cloud.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@EnableHystrix
@SpringCloudApplication
public class HystrixApp {

    public static void main( String[] args ){
        SpringApplication.run(HystrixApp.class);
        System.out.println("HystrixApp Hello World!" );
    }
}
