package com.jysemel.spring.cloud.sample.controller;

import com.jysemel.spring.cloud.facade.SponsorClient;
import com.jysemel.spring.cloud.facade.vo.HelloModel;
import com.jysemel.spring.cloud.sample.vo.CommonResponse;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@RestController
@RequestMapping("/index")
@DefaultProperties(defaultFallback = "defaultFallback")
public class IndexController {

    @Autowired
    private SponsorClient sponsorClient;

    @GetMapping("list")
    public CommonResponse<List<HelloModel>> getList(HttpServletRequest request) throws Exception {
        return sponsorClient.listHello();
    }

    @GetMapping("getTimeOut")
    public String getTimeOut() throws Exception {
        throw new Exception("test");
    }


    private String defaultFallback(){
        return "defaultFallback errorCallback";
    }
}