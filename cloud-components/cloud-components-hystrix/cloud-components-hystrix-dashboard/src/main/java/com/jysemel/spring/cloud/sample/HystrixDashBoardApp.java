package com.jysemel.spring.cloud.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@EnableHystrixDashboard
@SpringBootApplication
public class HystrixDashBoardApp {

    public static void main( String[] args ){
        SpringApplication.run(HystrixDashBoardApp.class);
        System.out.println("HystrixDashBoardApp Hello World!" );
    }
}
