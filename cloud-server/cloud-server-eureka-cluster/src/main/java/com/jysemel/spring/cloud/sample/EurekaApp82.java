package com.jysemel.spring.cloud.sample;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * eureka注册中心
 * @author jysemel
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaApp82 {

    public static void main( String[] args ){
        SpringApplication.run(EurekaApp82.class);
        System.out.println( "EurekaApp82 Hello World!" );
    }

}