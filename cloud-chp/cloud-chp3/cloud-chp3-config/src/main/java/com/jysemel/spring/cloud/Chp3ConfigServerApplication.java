package com.jysemel.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author jysemel
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableConfigServer
public class Chp3ConfigServerApplication {


	public static void main(String[] args) {
		SpringApplication.run(Chp3ConfigServerApplication.class, args);
	}

}