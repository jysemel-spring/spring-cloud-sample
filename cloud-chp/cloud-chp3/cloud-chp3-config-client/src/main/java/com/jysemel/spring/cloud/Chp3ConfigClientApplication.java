package com.jysemel.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author jysemel
 */
@EnableDiscoveryClient
@SpringBootApplication
public class Chp3ConfigClientApplication {


	public static void main(String[] args) {
		SpringApplication.run(Chp3ConfigClientApplication.class, args);
	}

}