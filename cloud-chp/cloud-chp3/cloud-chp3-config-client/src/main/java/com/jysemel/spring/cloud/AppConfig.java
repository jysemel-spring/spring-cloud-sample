package com.jysemel.spring.cloud;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello world!
 *
 */
@Configuration
@EnableAutoConfiguration
@RestController
public class AppConfig {

    @Value("${git.commit.user.email}") // git配置文件里的key
     String testName;

    @RequestMapping(value = "/hi")
    public String hi(){
        return testName;
    }
}
