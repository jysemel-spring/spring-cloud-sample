package com.jysemel.spring.cloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class HelloController {


    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/hello")
    public String sayHello(String name) {
        //调用生产者的服务返回信息
        return restTemplate.getForObject("http://CLOUD-CHP4-P1/hello", String.class);
    }

}