package com.jysemel.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author jysemel
 */
@EnableDiscoveryClient
@EnableZuulProxy
@SpringBootApplication
public class Chp2ZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(Chp2ZuulApplication.class, args);
	}

}