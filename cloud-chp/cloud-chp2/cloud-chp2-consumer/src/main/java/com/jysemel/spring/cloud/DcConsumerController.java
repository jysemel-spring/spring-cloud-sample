package com.jysemel.spring.cloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author jysemel
 */
@RestController
public class DcConsumerController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/dc")
    public String dc(){

        String resStr = restTemplate.getForObject("http://PROVIDER/dc",String.class);

        return resStr;
    }


}