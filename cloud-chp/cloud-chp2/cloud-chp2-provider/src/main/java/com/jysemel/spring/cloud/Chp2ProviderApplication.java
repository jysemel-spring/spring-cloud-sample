//package com.jysemel.spring.cloud;
//
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.rpc.discovery.EnableDiscoveryClient;
//
///**
// * @author jysemel
// */
//@EnableDiscoveryClient
//@SpringBootApplication
//public class Chp2ProviderApplication {
//
//	public static void main(String[] args) {
//		SpringApplication.run(Chp2ProviderApplication.class, args);
//	}
//
//}