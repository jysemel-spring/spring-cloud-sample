package com.jysemel.spring.cloud;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jysemel
 */
@Controller
public class ClientController {


    @GetMapping("dc")
    @ResponseBody
    public String getDc(){
        return "dc";
    }
}