package com.jysemel.spring.cloud.controller;

import com.jysemel.spring.cloud.controller.model.TestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BcController {

    @Autowired
    DiscoveryClient discoveryClient;

    @GetMapping("/bt")
    public Boolean bt(@RequestParam  TestDto testDto) {
        String services = "Services: " + discoveryClient.getServices();
        System.out.println(services);
        return true;
    }

}