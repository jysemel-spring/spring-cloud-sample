package com.jysemel.spring.cloud.controller.model;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class TestDto implements Serializable {

    @ApiModelProperty(value="用户名",name="username",example="xingguo")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
