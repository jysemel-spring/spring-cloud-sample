package com.jysemel.spring.cloud;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class ErrorMyFilter extends ZuulFilter {

    @Override
    public String filterType() {
        System.out.println("===================error filterType");
        return "error";
    }

    @Override
    public int filterOrder() {
        System.out.println("========================error filterOrder");
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        System.out.println("========================shouldFilter");
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        Object accessToken = request.getParameter("token");
        if (accessToken == null) {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            try {
                ctx.getResponse().getWriter().write("token is empty");
            } catch (Exception e) {
            }

            return null;
        }
        return null;
    }
}