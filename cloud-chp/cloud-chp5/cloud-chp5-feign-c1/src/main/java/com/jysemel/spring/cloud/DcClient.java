package com.jysemel.spring.cloud;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("feign-p1")
public interface DcClient {

    @GetMapping("/dc")
    String consumer();

}