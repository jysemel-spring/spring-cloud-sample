package com.jysemel.spring.cloud;

import org.springframework.web.bind.annotation.GetMapping;

public interface HelloService {

    @GetMapping("/dc")
    String hello();

}