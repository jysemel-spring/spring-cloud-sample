package com.jysemel.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RestController;

@EnableDiscoveryClient
@SpringBootApplication
public class Application {

    @RestController
    class HelloController implements HelloService {

        @Override
        public String hello() {
            return "hello ";
        }

    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}