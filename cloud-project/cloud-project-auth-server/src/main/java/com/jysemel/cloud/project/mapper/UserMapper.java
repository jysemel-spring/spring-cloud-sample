package com.jysemel.cloud.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jysemel.cloud.project.mapper.model.UserDO;

import java.util.List;

/**
 * @author jysemel
 */
public interface UserMapper extends BaseMapper<UserDO> {


    public List<UserDO> selectAll();
}