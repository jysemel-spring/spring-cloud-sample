package com.jysemel.cloud.project.controller;

import com.jysemel.cloud.project.controller.req.LoginVO;
import com.jysemel.cloud.project.service.AuthService;
import com.jysemel.spring.cloud.sample.vo.CommonResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author: jysemel
 * @Date: 2021/4/11 13:44
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 */
@RestController
@RequestMapping("auth")
@RequiredArgsConstructor
@Slf4j
public class AuthTokenController {

    @Autowired
    private AuthService authService;


    @PostMapping("/token")
    public CommonResponse loginByForm(String username, String password, HttpServletRequest request, HttpServletResponse response) throws IOException {
        return CommonResponse.success();
    }

    /**
     * 用户登录
     */
    @PostMapping("/login")
    public CommonResponse login(@RequestBody LoginVO login) {
        return CommonResponse.success();
    }

    /**
     * 用户注销登录、移除token信息
     */
    @PostMapping("/logout")
    public CommonResponse logout(HttpServletRequest request) {
        return CommonResponse.success();
    }

    /**
     * 当前登录用户信息
     */
    @GetMapping("/info")
    public CommonResponse currUser(HttpServletRequest request) {
        return null;
    }
}