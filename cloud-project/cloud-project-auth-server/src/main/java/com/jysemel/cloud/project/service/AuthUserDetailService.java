package com.jysemel.cloud.project.service;

import com.jysemel.cloud.project.mapper.UserMapper;
import com.jysemel.cloud.project.mapper.model.UserDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: jysemel
 * @Date: 2021/4/11 14:09
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 */
@Slf4j
@Service
public class AuthUserDetailService implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        List<UserDO> users = userMapper.selectAll();
        UserDO userDO = users.get(0);
        return new User(userDO.getUserName(), userDO.getPwd(),
                AuthorityUtils.commaSeparatedStringToAuthorityList("Role_admin"));

    }
}
