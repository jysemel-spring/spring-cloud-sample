package com.jysemel.cloud.project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: jysemel
 * @Date: 2021/4/11 12:15
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 */
@MapperScan("com.jysemel.cloud.project.mapper")
@SpringBootApplication
public class AuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }
}
