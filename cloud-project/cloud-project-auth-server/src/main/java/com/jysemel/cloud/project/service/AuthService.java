package com.jysemel.cloud.project.service;

import com.jysemel.cloud.project.mapper.UserMapper;
import com.jysemel.cloud.project.mapper.model.UserDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: jysemel
 * @Date: 2021/4/11 13:56
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 */
@Slf4j
@Service
public class AuthService {

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);

    private final static long EXPIRE = 12 * 60 * 60;

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserMapper userMapper;

    /**
     * 校验用户登录
     * @param account 登录账号
     * @param password 密码
     * @return 用戶信息
     */
    public UserDO login(String account, String password) {
        List<UserDO> userDOS = userMapper.selectAll();
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(account, password));
        return userDOS.get(0);
    }
}