package com.jysemel.cloud.project.mapper.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * des:
 * @author jysemel
 * @date 2019/03/24
 */
@Data
@TableName("user_info")
public class UserDO {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("userName")
    private String userName;
    @TableField("pwd")
    private String pwd;
}