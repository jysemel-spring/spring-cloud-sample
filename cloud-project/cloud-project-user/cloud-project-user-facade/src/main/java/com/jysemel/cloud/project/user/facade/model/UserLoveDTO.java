package com.jysemel.cloud.project.user.facade.model;

import lombok.Data;

/**
 * @author jysemel
 */
@Data
public class UserLoveDTO {

    private String name;
    private String love;
}