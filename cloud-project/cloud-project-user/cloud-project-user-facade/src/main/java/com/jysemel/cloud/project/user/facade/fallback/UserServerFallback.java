package com.jysemel.cloud.project.user.facade.fallback;

import com.jysemel.cloud.project.user.facade.UserServer;
import com.jysemel.cloud.project.user.facade.model.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author jysemel
 */
@Slf4j
@Component
public class UserServerFallback implements UserServer {

    @Override
    public List<UserDTO> queryUserList(@RequestParam(name = "userCode") String userCode) {
        log.error("发生异常信息....................");
        return null;
    }
}