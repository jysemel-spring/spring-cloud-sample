package com.jysemel.cloud.project.user.facade;

import com.jysemel.cloud.project.user.facade.model.UserDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author jysemel
 */
public interface UserServer {


    /**
     * 查询列表
     * @return
     */
    @GetMapping(value = "api/queryUserList",consumes = MediaType.APPLICATION_JSON_VALUE,produces =MediaType.APPLICATION_JSON_VALUE )
    List<UserDTO> queryUserList(@RequestParam(name = "userCode") String userCode);
}
