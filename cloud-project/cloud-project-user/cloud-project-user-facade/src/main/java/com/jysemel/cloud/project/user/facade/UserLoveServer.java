package com.jysemel.cloud.project.user.facade;

import com.jysemel.cloud.project.user.facade.model.UserDTO;
import com.jysemel.cloud.project.user.facade.model.UserLoveDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author jysemel
 */
public interface UserLoveServer {

    /**
     *查询用户爱好列表
     * @return
     */
    @PostMapping("/query/UserLoveList")
    public List<UserLoveDTO> queryUserLoveList(@RequestBody UserDTO userDTO);
}
