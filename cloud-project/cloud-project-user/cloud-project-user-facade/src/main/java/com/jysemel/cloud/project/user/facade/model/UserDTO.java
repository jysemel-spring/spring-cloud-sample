package com.jysemel.cloud.project.user.facade.model;

import lombok.Data;

/**
 * @author jysemel
 */
@Data
public class UserDTO {

    private String name;
    private String pwd;
}