package com.jysemel.cloud.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author jysemel
 */
@EnableDiscoveryClient
@SpringBootApplication
public class UserApp {

    public static void main( String[] args ){
        SpringApplication.run(UserApp.class);
        System.out.println("UserApp Hello World!" );
    }
}
