package com.jysemel.cloud.project.user.controller;

import com.alibaba.fastjson.JSON;
import com.jysemel.cloud.project.user.facade.UserLoveServer;
import com.jysemel.cloud.project.user.facade.model.UserDTO;
import com.jysemel.cloud.project.user.facade.model.UserLoveDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author jysemel
 */
@Slf4j
@RequestMapping("/user")
@RestController
public class UserLoveServerController implements UserLoveServer {

    @Override
    public List<UserLoveDTO> queryUserLoveList(@RequestBody UserDTO userDTO) {
        log.info(JSON.toJSONString(userDTO));
        return null;
    }
}
