package com.jysemel.cloud.project.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jysemel.cloud.project.user.mapper.model.HelloModel;

import java.util.List;

/**
 * @author jysemel
 */
public interface HelloMapper extends BaseMapper<HelloModel> {


    public List<HelloModel> selectAll();
}