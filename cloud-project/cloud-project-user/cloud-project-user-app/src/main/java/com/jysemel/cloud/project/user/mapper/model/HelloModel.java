package com.jysemel.cloud.project.user.mapper.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/03/24
 */
@Data
@TableName("hello_model")
public class HelloModel {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("title")
    private String title;
    @TableField("text")
    private String text;
}