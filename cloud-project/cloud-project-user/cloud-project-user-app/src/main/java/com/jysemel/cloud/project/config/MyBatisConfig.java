package com.jysemel.cloud.project.config;

import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/03/24
 */
@Configuration
@MapperScan(basePackages = {"com.jysemel.cloud.project.user.mapper.**"})
public class MyBatisConfig {

    /**
     * mybatis-plus SQL执行效率插件【生产环境可以关闭】
     */
    @Bean
    public PerformanceInterceptor performanceInterceptor() {

        return new PerformanceInterceptor();
    }
}