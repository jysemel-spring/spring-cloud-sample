package com.jysemel.cloud.project.user.controller;

import com.jysemel.cloud.project.user.facade.UserServer;
import com.jysemel.cloud.project.user.facade.model.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jysemel
 */
@Slf4j
@RequestMapping("/user")
@RestController
public class UserServerController implements UserServer {


    @Override
    @GetMapping("api/queryUserList")
    public List<UserDTO> queryUserList(@RequestParam(name = "userCode") String userCode) {
        log.info("--------------------------"+userCode);
        return new ArrayList<UserDTO>();
    }

}