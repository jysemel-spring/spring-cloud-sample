package com.jysemel.cloud.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author: jysemel
 * @Date: 2021/4/11 09:48
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableHystrixDashboard
public class DashBoardApp {

    public static void main( String[] args ){
        SpringApplication.run(DashBoardApp.class);
        System.out.println("DashBoardApp Hello World!" );
    }
}