package com.jysemel.cloud.project.config;

import com.jysemel.cloud.project.controller.AuthHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * @author: jysemel
 * @Date: 2021/4/12 13:52
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc
 */
@Configuration
public class RouterFunctionConfig {

    @Bean
    public RouterFunction<ServerResponse> authLogin(AuthHandler handler) {
        return RouterFunctions.route(
                RequestPredicates.POST("/auth/login")
                .and(RequestPredicates.accept(MediaType.APPLICATION_FORM_URLENCODED))
                ,handler::login);
    }


}
