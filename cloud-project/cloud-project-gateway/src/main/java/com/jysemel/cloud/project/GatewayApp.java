package com.jysemel.cloud.project;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author jysemel
 */
@SpringBootApplication
public class GatewayApp {

    public static void main( String[] args ){
        SpringApplication.run(GatewayApp.class);
        System.out.println("GatewayApp Hello World!" );
    }
}