package com.jysemel.cloud.project.fallback;

import com.jysemel.spring.cloud.sample.vo.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jysemel
 */
@Slf4j
@RestController
public class FallBackController {


    @RequestMapping("/fallBack")
    public CommonResponse fallBack(){
        log.info("fallBack-------------");
        return CommonResponse.error("请稍后重试....");
    }
}