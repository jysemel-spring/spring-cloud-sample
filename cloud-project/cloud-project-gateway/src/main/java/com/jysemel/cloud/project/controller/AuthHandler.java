package com.jysemel.cloud.project.controller;

import com.jysemel.spring.cloud.sample.vo.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author: jysemel
 * @Date: 2021/4/12 11:59
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc  登录
 */
@Slf4j
@Component
public class AuthHandler {

    /**
     * handle：
     * @param request
     * @return
     */
    public Mono<ServerResponse> login(ServerRequest request){
        ServerWebExchange exchange = request.exchange();
        exchange.getFormData();

        Map<String, Object> params = new HashMap<>();
        String randomStr = UUID.randomUUID().toString().replaceAll("-", "");
        params.put("randomStr", randomStr);
        CommonResponse commonResponse = CommonResponse.success(randomStr);
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        log.info("------------------------------");
        return ServerResponse.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .header("randomstr", randomStr)
                .body(BodyInserters.fromValue(commonResponse));
    }
}