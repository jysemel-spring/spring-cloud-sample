package com.jysemel.cloud.project.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;

/**
 * @author: jysemel
 * @Date: 2021/4/11 14:51
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc 认证
 */
@Slf4j
@Component
public class AuthGatewayFilterFactory  extends AbstractNameValueGatewayFilterFactory {

    /**
     * 白名单
     */
    private static final String[] whiteList = {
            "/auth/login",
            "/auth/user/token",
            "/v2/api-docs",
            "/auth/v2/api-docs"
    };

    @Override
    public GatewayFilter apply(NameValueConfig config) {
        return new AuthFilter();
    }

    class AuthFilter implements GatewayFilter, Ordered{
        @Override
        public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
            String url = exchange.getRequest().getURI().getPath();
            log.info("认证用户访问地址URL: {}",url);
            if (Arrays.asList(whiteList).contains(url)) {
                return chain.filter(exchange);
            }
            // 设置userId到request里，后续根据userId，获取用户信息
            ServerHttpRequest req = exchange.getRequest().mutate()
//				.header(Constants.CURRENT_ID, userId)
//				.header(Constants.CURRENT_USERNAME, account)
                    .build();
            // 放行
//		return chain.filter(exchange.mutate().request(req).build());
            Mono<Void> r = chain.filter(exchange);
            log.info("认证通过-----------------------------");
            return r;
        }

        @Override
        public int getOrder(){
            return 0;
        }
    }
}