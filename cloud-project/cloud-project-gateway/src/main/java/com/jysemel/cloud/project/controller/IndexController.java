package com.jysemel.cloud.project.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jysemel
 */
@RestController
public class IndexController {

    @Value("${test:80}")
    private String blog;

    @GetMapping
    public String getInfo(){
        return "blog info" + blog;
    }
}