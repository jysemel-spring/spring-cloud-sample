package com.jysemel.cloud.project.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author: jysemel
 * @Date: 2021/4/11 15:42
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc
 */
@Slf4j
public class AfterLogGlobalFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("访问网关结束-----------------------");
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}