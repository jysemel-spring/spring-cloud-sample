package com.jysemel.cloud.project.config;

import com.jysemel.cloud.project.filter.PreLogGlobalFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * @author: jysemel
 * @Date: 2021/4/11 11:08
 * @微信 jysemel
 * @gitee https://gitee.com/kcnf
 * @wb https://my.oschina.net/kcnf
 * @wb http://jysemel.xyz
 * @desc 注入全局过滤器
 */
@Slf4j
@Configuration
public class FilterConfig {

    @Bean
    @Order(-999)
    public GlobalFilter preLogGlobalFilter() {

        return new PreLogGlobalFilter();
    }
}