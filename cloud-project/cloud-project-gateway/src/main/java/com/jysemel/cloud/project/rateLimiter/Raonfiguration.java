package com.jysemel.cloud.project.rateLimiter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jysemel
 */
@Slf4j
@Configuration
public class Raonfiguration {

    /**
     * 按照Path限流
     * @return key
     */
//    @Bean
//    public KeyResolver pathKeyResolver() {
//        return exchange -> Mono.just(
//                exchange.getRequest()
//                        .getPath()
//                        .toString()
//        );
//    }

    @Bean
    public HostAddrKeyResolver hostAddrKeyResolver() {
        return new HostAddrKeyResolver();
    }
}