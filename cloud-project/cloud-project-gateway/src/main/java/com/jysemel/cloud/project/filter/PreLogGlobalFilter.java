package com.jysemel.cloud.project.filter;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * 网关日志入口
 * @author jysemel
 */
@Slf4j
public class PreLogGlobalFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //前置过滤
        String uuid = UUID.randomUUID().toString();
        MDC.put("TRACE_LOG_ID",uuid);
        log.info("访问网关开始-----------------------");
        exchange.getAttributes().put("START_TIME", System.currentTimeMillis());
        //执行逻辑
        Mono<Void> re = chain.filter(exchange).then( Mono.fromRunnable(() -> {
            Long startTime = exchange.getAttribute("START_TIME");
            if (startTime != null) {
                Long executeTime = (System.currentTimeMillis() - startTime);
                log.info("====================="+exchange.getRequest().getURI().getRawPath() + " : " + executeTime + "ms");
                log.info("访问网关结束-----------------------");
                MDC.clear();
            }
        }));
        //后置过滤
        return re;
    }

    @Override
    public int getOrder() {
        return 0;
    }
}