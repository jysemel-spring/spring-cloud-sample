package com.jysemel.cloud.project.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;


/**
 * @author jysemel
 */
public class BlogFeignConfiguration {

    @Bean
    public Logger.Level level(){
        return Logger.Level.FULL;
    }
}
