package com.jysemel.cloud.project.controller;

import com.jysemel.cloud.project.order.facade.IOrderServer;
import com.jysemel.cloud.project.order.facade.model.OrderDTO;
import com.jysemel.cloud.project.rpc.UserServerImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * @author jysemel
 */
@Slf4j
@RestController
public class IndexController {

    @Value("${test:80}")
    private String test;

    @Autowired
    private UserServerImpl userServer;

    @DubboReference
    private IOrderServer orderServer;

    @Value("${ddd:80}")
    private String ddd;


    @GetMapping
    public String getInfo(@RequestParam(name = "userCode") String userCode){
        userServer.queryUserList(userCode);
        return "blog info" + test+"------"+ddd;
    }

    @PostMapping("/query")
    public String queryUserLoveList(@RequestBody OrderDTO orderDTO){
        orderServer.queryOrderList(orderDTO);
        return "blog info" + test+"------"+ddd;
    }
}