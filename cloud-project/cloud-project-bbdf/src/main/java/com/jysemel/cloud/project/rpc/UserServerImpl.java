package com.jysemel.cloud.project.rpc;

import com.jysemel.cloud.project.config.BlogFeignConfiguration;
import com.jysemel.cloud.project.user.facade.UserServer;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author jysemel
 */
@FeignClient(name = "project-user",path = "/user",configuration = BlogFeignConfiguration.class)
public interface UserServerImpl extends UserServer {
}