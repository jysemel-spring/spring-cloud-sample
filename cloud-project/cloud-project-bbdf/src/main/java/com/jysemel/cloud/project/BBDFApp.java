package com.jysemel.cloud.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author jysemel
 */
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class BBDFApp {

    public static void main( String[] args ){
        SpringApplication.run(BBDFApp.class);
        System.out.println("BBDFApp Hello World!" );
    }
}