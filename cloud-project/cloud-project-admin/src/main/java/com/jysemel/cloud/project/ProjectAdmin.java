package com.jysemel.cloud.project;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jysemel
 */
@EnableAdminServer
@SpringBootApplication
public class ProjectAdmin {


    public static void main( String[] args ){
        SpringApplication.run(ProjectAdmin.class);
        System.out.println("ProjectAdmin Hello World!" );
    }
}