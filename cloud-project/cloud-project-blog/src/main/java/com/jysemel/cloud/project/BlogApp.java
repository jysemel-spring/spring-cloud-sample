package com.jysemel.cloud.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author jysemel
 */
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class BlogApp {

    public static void main( String[] args ){
        SpringApplication.run(BlogApp.class);
        System.out.println("BlogApp Hello World!" );
    }

    /**
     * 引入ribbon 负载
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
