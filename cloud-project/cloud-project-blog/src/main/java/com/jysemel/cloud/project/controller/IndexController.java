package com.jysemel.cloud.project.controller;

import com.jysemel.cloud.project.rpc.UserLoveServerImpl;
import com.jysemel.cloud.project.rpc.UserServerImpl;
import com.jysemel.cloud.project.user.facade.model.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * @author jysemel
 */
@Slf4j
@RequestMapping("/blog")
@RestController
public class IndexController {

    @Value("${test:80}")
    private String test;

    @Autowired
    private UserServerImpl userServer;

    @Autowired
    private UserLoveServerImpl userLoveServer;

    @Value("${ddd:80}")
    private String ddd;


    @GetMapping
    public String getInfo(@RequestParam(name = "userCode") String userCode){
        userServer.queryUserList(userCode);
//        try {
//            Thread.sleep(TimeUnit.SECONDS.toMillis(1));
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        log.info("-------------------");
        return "blog info" + test+"------"+ddd;
    }

    @PostMapping("/query")
    public String queryUserLoveList(@RequestBody UserDTO userDTO){
        userLoveServer.queryUserLoveList(userDTO);
        return "blog info" + test+"------"+ddd;
    }
}