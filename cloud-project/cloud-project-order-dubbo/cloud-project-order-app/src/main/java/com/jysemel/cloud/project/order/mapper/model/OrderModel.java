package com.jysemel.cloud.project.order.mapper.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author jysemel
 */
@Data
public class OrderModel {

    private String proName;
    private BigDecimal amount;

}