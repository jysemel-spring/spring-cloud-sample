package com.jysemel.cloud.project.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jysemel.cloud.project.order.mapper.model.OrderModel;

import java.util.List;

/**
 * @author jysemel
 */
public interface OrderMapper extends BaseMapper<OrderModel> {


    public List<OrderModel> selectAll();
}
