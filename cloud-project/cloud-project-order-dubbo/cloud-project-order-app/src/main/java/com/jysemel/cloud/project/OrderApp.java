package com.jysemel.cloud.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author jysemel
 */
@EnableDiscoveryClient
@SpringBootApplication
public class OrderApp {

    public static void main( String[] args ){
        SpringApplication.run(OrderApp.class);
        System.out.println("OrderApp Hello World!" );
    }
}