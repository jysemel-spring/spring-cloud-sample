package com.jysemel.cloud.project.order.rpc;

import com.jysemel.cloud.project.order.facade.IOrderServer;
import com.jysemel.cloud.project.order.facade.model.OrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.List;

/**
 * @author jysemel
 */
@Slf4j
@DubboService
public class OrderServerImpl implements IOrderServer {

    @Override
    public List<OrderDTO> queryOrderList(OrderDTO orderDTO) {
        log.info("--------------@DubboService");
        return null;
    }
}
