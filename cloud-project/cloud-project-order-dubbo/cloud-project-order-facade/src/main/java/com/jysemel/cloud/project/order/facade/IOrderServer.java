package com.jysemel.cloud.project.order.facade;

import com.jysemel.cloud.project.order.facade.model.OrderDTO;

import java.util.List;

/**
 * @author jysemel
 */
public interface IOrderServer {

    List<OrderDTO> queryOrderList(OrderDTO orderDTO);
}