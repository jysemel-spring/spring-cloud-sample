package com.jysemel.spring.cloud.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/04/04
 */
@EnableDiscoveryClient
@EnableConfigServer
@SpringBootApplication
public class ConfigApp {

    public static void main( String[] args ){
        SpringApplication.run(ConfigApp.class);
        System.out.println("ConfigApp Hello World!" );
    }

}