package com.jysemel.spring.cloud.sample;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author jysemel
 */
@EnableEurekaClient
@SpringBootApplication
public class SponsorApp {


    public static void main( String[] args ){
        SpringApplication.run(SponsorApp.class);
        System.out.println("SponsorApp Hello World!" );
    }
}