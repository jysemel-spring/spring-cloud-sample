//package com.jysemel.spring.cloud.sample;
//
//
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
//import org.springframework.context.annotation.Profile;
//
///**
// * @author jysemel
// */
//@EnableEurekaClient
//@SpringBootApplication
//public class SponsorApp7001 {
//
//
//    public static void main( String[] args ){
//        SpringApplication.run(SponsorApp7001.class);
//        System.out.println("SponsorApp7001 Hello World!" );
//    }
//}