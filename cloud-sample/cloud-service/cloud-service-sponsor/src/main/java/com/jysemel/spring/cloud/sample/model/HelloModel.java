package com.jysemel.spring.cloud.sample.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/03/24
 */
@Data
@TableName("hello_model")
public class HelloModel {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("title")
    private String title;
    @TableField("text")
    private String text;
}