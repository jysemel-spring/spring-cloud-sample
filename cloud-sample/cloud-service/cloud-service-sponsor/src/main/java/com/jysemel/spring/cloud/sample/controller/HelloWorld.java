package com.jysemel.spring.cloud.sample.controller;

import com.alibaba.fastjson.JSON;
import com.jysemel.spring.cloud.sample.model.HelloModel;
import com.jysemel.spring.cloud.sample.service.HelloService;
import com.jysemel.spring.cloud.sample.vo.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

/**
 * des:
 * @author jysemel
 * @date 2019/03/24
 */
@Slf4j
@RestController
public class HelloWorld {

    @Autowired
    private HelloService helloService;

    @RequestMapping("/")
    public String Index() {
        HelloModel model = new HelloModel();
        model.setText(UUID.randomUUID().toString());
        model.setTitle(UUID.randomUUID().toString());
        helloService.insert(model);
        return "Hello World";
    }

    @RequestMapping("/list")
    public CommonResponse<List<HelloModel>> List() {
        MDC.put("traceId", UUID.randomUUID().toString());
        List<HelloModel> modelList = helloService.selectAll();
        log.info("modelLis： {}", JSON.toJSONString(modelList));
        MDC.clear();
        return new CommonResponse(200,"success",modelList);
    }

}