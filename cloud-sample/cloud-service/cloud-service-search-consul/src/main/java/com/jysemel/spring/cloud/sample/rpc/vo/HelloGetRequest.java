package com.jysemel.spring.cloud.sample.rpc.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * des:
 *
 * @author jysemel
 * @date 2019/03/24
 */
@Data
@AllArgsConstructor
public class HelloGetRequest {

    private Long userId;
}