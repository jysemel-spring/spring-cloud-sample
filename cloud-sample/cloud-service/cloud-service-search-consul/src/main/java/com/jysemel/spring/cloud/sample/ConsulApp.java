package com.jysemel.spring.cloud.sample;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author jysemel
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ConsulApp {

    public static void main( String[] args ){
        SpringApplication.run(ConsulApp.class);
        System.out.println("ConsulApp Hello World!" );
    }

}